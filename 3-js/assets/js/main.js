// jQuery
// $(document).ready(function() {
// code
// });

// // Vanilla JS

window.onload = function () {
  let botao = document.getElementById("menu-button");

  botao.addEventListener("click", () => {
    let menu = document.getElementById("menu");

    menu.classList.toggle("-active");
  });

  let itens = document.querySelectorAll(".accordion .item");

  for (let i = 0; i < itens.length; i++) {
    itens
      .item(i)
      .querySelector(".title")
      .addEventListener("click", () => {
        const ativo = document.querySelector(".accordion .item.-active");

        ativo.classList.remove("-active");

        itens.item(i).classList.add("-active");
      });
  }

  const getImage = () => {
    const urlImage =
      "https://en.wikipedia.org/w/api.php?origin=*&format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=Alber%20Einstein";

    fetch(urlImage, {
      headers: new Headers({
        "Content-Type": "application/json; charset=UTF-8",
        Origin: "*",
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        const extract = document.querySelector(".wiki .extract");
        extract.innerHTML = response.query.pages[736].extract;
      });
  };

  let openModal = document.getElementById("button-modal");
  openModal.addEventListener("click", () => {
    let modal = document.querySelector(".modal-wiki");
    modal.classList.toggle("-active");
  });

  let closeModal = document.getElementById("close-modal");
  closeModal.addEventListener("click", () => {
    let modal = document.querySelector(".modal-wiki");
    modal.classList.toggle("-active");
  });
  getImage();
};
